var app = angular.module('app', []);

app.controller('miControlador', function ($scope, $http) {
	$scope.success = false;
	$scope.error = false;

	$scope.listarDatos = function () {
		$http.post('ObtenerDatos.php').then(function (data) {
			$scope.DatosNombre = data.data;
		});
	};

	$scope.cerrarModal = function () {

		//var ModalAg = new bootstrap.Modal(document.getElementById('nuevoDatoModal'), { keyboard: false });
		//var ModalAg = document.getElementById('nuevoDatoModal');
		//console.log(ModalAg);
		//ModalAg.modal('hide');
		const modal = document.getElementById('nuevoDatoModal');

		// change state like in hidden modal
		modal.classList.remove('show');
		modal.setAttribute('aria-hidden', 'true');
		modal.setAttribute('style', 'display: none');
		const modalBackdrops = document.getElementsByClassName('modal-backdrop');
		document.body.removeChild(modalBackdrops[0]);
	};

	$scope.nuevoDato = function () {
		$scope.TituloModal = 'Agregar Datos';
		$scope.TextoBtn = 'Guardar';

		abrirModalFuncion();
	};

	$scope.enviarDatos = function () {
		//$scope.accion = "insertar";

		const Parametros = {
			'txtNombre': $scope.txtNombre,
			'txtApellido': $scope.txtApellido,
			'txtCodigo': $scope.txtCodigo,
			'SqlQuery': $scope.TextoBtn
		};

		$http({
			method: "POST",
			url: "ProcesosSQL.php",
			data: Parametros
		}).then(function (data) {
			const Mensaje = data.data.MensajeOk;

			$scope.cerrarModal();
			//ModalAg.modal('hide');
			$scope.listarDatos();
		});
	};

	$scope.listarUnDato = function (id) {
		$scope.accion = "listarundato";
		$http({
			method: "POST",
			url: "ProcesosSQL.php",
			data: { 'txtCodigo': id, 'SqlQuery': $scope.accion }
		}).then(function (data) {
			const Datos = data.data;

			$scope.txtNombre = Datos.first_name;
			$scope.txtApellido = Datos.last_name;
			$scope.txtCodigo = id;
			$scope.TituloModal = 'Modificar Datos';
			$scope.TextoBtn = 'Modificar';

			abrirModalFuncion();
		});
	};

	$scope.EliminarDato = function (id) {
		$scope.accion = "Eliminar";
		Swal.fire({
			title: '¿Desea Eliminar el Registro?',
			text: "Este proceso es irreversible",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, Eliminar',
			cancelButtonText: 'No, Cancelar!',
		}).then((result) => {
			if (result.isConfirmed) {
				$http({
					method: "POST",
					url: "ProcesosSQL.php",
					data: { 'txtCodigo': id, 'SqlQuery': $scope.accion }
				}).then(function (data) {
					$scope.listarDatos();
				});
			} else if (result.dismiss===Swal.DismissReason.cancel) {
				Swal.fire('No se ha realizado ningun cambio', '', 'info')
			}

		});
	};
});

function abrirModalFuncion() {
	var modalAgregar = new bootstrap.Modal(document.getElementById('nuevoDatoModal'),
		{
			keyboard: false,
			backdrop: 'static'
		});

	modalAgregar.show();
}