<div class="modal fade" id="nuevoDatoModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="nuevoDatoModalLabel">{{TituloModal}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="post" ng-submit="enviarDatos()">
                <input type="hidden" id="txtCodigo" ng-model="txtCodigo">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="txtNombre" class="form-label">Nombre</label>
                        <input type="text" class="form-control" ng-model="txtNombre" id="txtNombre" placeholder="Digita tu Nombre">
                    </div>
                    <div class="mb-3">
                        <label for="txtApellido" class="form-label">Apellido</label>
                        <input type="text" class="form-control" ng-model="txtApellido" id="txtApellido" placeholder="Digita tu Apellido">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary btn-sm"> <i class="fas fa-save"></i> {{TextoBtn}}</button>
                </div>
            </form>
        </div>
    </div>
</div>