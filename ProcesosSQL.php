<?php
include('Conexion/Conexion.php');

$form_data = json_decode(file_get_contents("php://input"));

$Salida = array();

if ($form_data->SqlQuery == 'listarundato') {
    $query = "SELECT * FROM tbl_sample WHERE id='" . $form_data->txtCodigo . "'";
    $statement = $Cnn->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    foreach ($result as $row) {
        $Salida['first_name'] = $row['first_name'];
        $Salida['last_name'] = $row['last_name'];
    }
} elseif ($form_data->SqlQuery == 'Guardar') {
    $txtNombre = ($form_data->txtNombre != '') ? $form_data->txtNombre : ' ';
    $txtApellido = ($form_data->txtApellido != '') ? $form_data->txtApellido : ' ';

    $data = array(
        ':txtNombre'        =>    $txtNombre,
        ':txtApellido'        =>    $txtApellido
    );

    $query = "INSERT INTO tbl_sample (first_name, last_name) 
                VALUES (:txtNombre, :txtApellido)";
    $statement = $Cnn->prepare($query);
    if ($statement->execute($data)) {
        $message = 'Datos Agregados';
    }

    //$Salida = array('MensajeOk' => 'Ok');
    $Salida['MensajeOk'] = "Ok";
}elseif ($form_data->SqlQuery == 'Modificar') {
    $txtNombre = ($form_data->txtNombre != '') ? $form_data->txtNombre : ' ';
    $txtApellido = ($form_data->txtApellido != '') ? $form_data->txtApellido : ' ';
    $txtCodigo = ($form_data->txtCodigo != '') ? $form_data->txtCodigo : ' ';

    $data = array(
        ':txtCodigo'        =>    $txtCodigo,
        ':txtNombre'        =>    $txtNombre,
        ':txtApellido'        =>    $txtApellido
    );

    $query = "UPDATE tbl_sample 
                SET 
                first_name = :txtNombre, 
                last_name=:txtApellido
                WHERE id=:txtCodigo";
    $statement = $Cnn->prepare($query);
    if ($statement->execute($data)) {
        $message = 'Datos Actualizados';
    }

    //$Salida = array('MensajeOk' => 'Ok');
    $Salida['MensajeOk'] = "Ok";
}elseif ($form_data->SqlQuery == 'Eliminar') {
    $txtCodigo = ($form_data->txtCodigo != '') ? $form_data->txtCodigo : ' ';

    $data = array(
        ':txtCodigo'        =>    $txtCodigo
    );

    $query = "DELETE FROM tbl_sample WHERE id=:txtCodigo";
    $statement = $Cnn->prepare($query);
    if ($statement->execute($data)) {
        $message = 'Datos Eliminados';
    }

    $Salida['MensajeOk'] = "Ok";
}

echo json_encode($Salida);
